const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = (_, argv) => ({
  entry: {
    app: './src/index.jsx',
  },
  resolve: { extensions: ['.js', '.jsx'] },
  module: {
    rules: [
      { // scripts
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader?cacheDirectory=true',
        },
      },
      { // css components
        test: /\.s?[ac]ss$/,
        exclude: /vendors\.scss$/,
        use: [
          argv.mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true,
              sourceMap: true,
              localIdentName: '[path][name]__[local]--[hash:base64:5]',
            },
          },
          'sass-loader',
        ],
      },
      { // css node_modules/vendors/globals
        test: /vendors\.scss$/,
        use: [
          argv.mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
        },
      },
    ],
  },
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: 'assets/scripts/[name].js',
    chunkFilename: 'assets/scripts/[name].js',
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './dist/',
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        extractVendorsStyles: {
          test: /vendors\.s?css$/,
          name: 'vendors',
          chunks: 'all',
          enforce: false,
        },
      },
    },
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  plugins: [
    new webpack.EnvironmentPlugin(),
    new MiniCssExtractPlugin({
      filename: 'assets/styles/[name].css',
      chunkFilename: 'assets/styles/[name].css',
    }),
    new HtmlWebpackPlugin({
      title: 'My App',
      filename: path.join(__dirname, '/dist/index.html'),
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(argv.mode)
    }),
  ],
});
