module.exports = {
    "extends": "airbnb",
    "env": {
        "browser": true
    },
    "rules": {
        "react/prop-types": 0,
        "jsx-a11y/heading-has-content": 0,
        "no-underscore-dangle": [2, { "allow": ["__schema", "__typename"] }]
    }
};