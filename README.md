# Github Stars

Comecei utilizando apenas redux e redux-observable com a api v3, tentando estruturar o projeto de forma modular.
Essa branch (graphql) foi refatora para utilizar a api v4 do github e possui dependencias apollo-client e graphql. Como não sabia que a v2 do apollo-client
funciona sem redux acabei integrando ambos de forma desnecessária, tornando o código das actions um pouco confuso.

Demo: https://guilima.bitbucket.io/

## Instructions
Dev environment
- npm install
- npm run dev

"Prod environment"
- npm install
- npm run build
- cd dist
- start a local server

## Technologies


React, redux, webpack and babel.
