import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import HomeModule from './modules/home/home.module';
import NotFound from './modules/not-found.component';
import './vendors.scss';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={HomeModule} />
    <Route component={NotFound} />
  </Switch>
);

export default hot(module)(Routes);
