import { combineEpics } from 'redux-observable';
import home from './modules/home/home.epic';
import auth from './modules/auth/auth.epic';

export default combineEpics(
  home,
  auth,
);
