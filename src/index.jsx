import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import rootEpic from './epics';
import rootReducer from './reducers';
import Auth from './modules/auth/auth.container';

import Routes from './routes';

const root = document.createElement('div');
const epicMiddleware = createEpicMiddleware();

document.body.appendChild(root);
const persistedState = localStorage.getItem('auth') ? { auth: JSON.parse(localStorage.getItem('auth')) } : {};
const store = createStore(rootReducer, persistedState, composeWithDevTools(
  applyMiddleware(epicMiddleware),
));

store.subscribe(() => {
  if (!store.getState().auth.token) { return; }
  localStorage.setItem('auth', JSON.stringify(store.getState().auth));
});

epicMiddleware.run(rootEpic);
ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <Auth>
        <Routes />
      </Auth>
    </BrowserRouter>
  </Provider>
), root);
