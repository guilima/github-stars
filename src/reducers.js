import { combineReducers } from 'redux';
import home from './modules/home/home.reducer';
import auth from './modules/auth/auth.reducer';

export default combineReducers({
  home,
  auth,
});
