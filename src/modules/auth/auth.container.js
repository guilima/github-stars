import { connect } from 'react-redux';
import { requestUserAuth } from './auth.action';
import Auth from './auth.component';

const mapStateToProps = state => ({ auth: state.auth });

const actions = { requestUserAuth };

export default connect(mapStateToProps, actions)(Auth);
