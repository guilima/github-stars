const initialState = {
  myUser: {},
  token: '',
  error: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RECEIVE_USER_AUTH':
      return { ...state, token: action.payload, error: false };
    case 'RECEIVE_MYUSER':
      return { ...state, myUser: action.payload };
    case 'ERROR_USER_AUTH':
      return { ...state, error: action.payload, token: '' };
    default:
      return state;
  }
};

export default authReducer;
