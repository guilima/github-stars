import { combineEpics } from 'redux-observable';
import { fetchUserAuthEpic, fetchMyUserEpic } from './auth.action';

export default combineEpics(
  fetchUserAuthEpic,
  fetchMyUserEpic,
);
