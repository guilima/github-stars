import React, { Component, Fragment } from 'react';

export default class Auth extends Component {
  componentDidMount() {
    const { requestUserAuth } = this.props;
    const code = window.location.search.replace('?code=', '');

    if (code) {
      requestUserAuth(code);
      window.history.replaceState(null, null, window.location.pathname);
    }
  }

  render() {
    const { children } = this.props;
    return (
      <Fragment>
        {children}
      </Fragment>
    );
  }
}
