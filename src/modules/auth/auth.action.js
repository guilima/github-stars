import { ofType } from 'redux-observable';
import { of } from 'rxjs';
import {
  switchMap,
  map,
  catchError,
} from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

export const REQUEST_MYUSER = 'REQUEST_MYUSER';
export const RECEIVE_MYUSER = 'RECEIVE_MYUSER';
export const REQUEST_USER_AUTH = 'REQUEST_USER_AUTH';
export const RECEIVE_USER_AUTH = 'RECEIVE_USER_AUTH';
export const ERROR_USER_AUTH = 'ERROR_USER_AUTH';

export const requestMyUser = token => ({
  type: REQUEST_MYUSER,
  payload: token,
});

export const receiveMyUser = payload => ({
  type: RECEIVE_MYUSER,
  payload,
});

export const requestUserAuth = code => ({
  type: REQUEST_USER_AUTH,
  payload: code,
});

export const receiveUserAuth = payload => ({
  type: RECEIVE_USER_AUTH,
  payload,
});

export const onUserAuthError = payload => ({
  type: ERROR_USER_AUTH,
  payload,
});

export const fetchMyUserEpic = action$ => action$.pipe(
  ofType(RECEIVE_USER_AUTH),
  switchMap(action => ajax.getJSON('https://api.github.com/user', { Authorization: `token ${action.payload}` }).pipe(
    map(response => receiveMyUser(response)),
  )),
);

// Need to change redirect path on github dev app settings to works local
export const fetchUserAuthEpic = action$ => action$.pipe(
  ofType(REQUEST_USER_AUTH),
  switchMap(action => ajax.getJSON(process.env.NODE_ENV === 'production'
    ? `https://github-star-oauth.herokuapp.com/oauth/${action.payload}`
    : `http://localhost:8081/oauth/${action.payload}`).pipe(
    map(response => receiveUserAuth(response)),
    catchError(error => of(onUserAuthError(error))),
  )),
);
