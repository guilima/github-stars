import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';
import { ofType } from 'redux-observable';
import {
  switchMap,
  map,
  debounceTime,
  filter,
} from 'rxjs/operators';
import { from } from 'rxjs';
import fragment from '../../shared/fragmentTypes.json';

const filterSchema = fragment.data.__schema.types.filter(
  type => type.kind === 'UNION' || type.kind === 'INTERFACE',
);

fragment.data.__schema.types = filterSchema;

export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';
export const REQUEST_USER_DATA = 'REQUEST_USER_DATA';
export const RECEIVE_USER_DATA = 'RECEIVE_USER_DATA';

export const handleSearchEvent = inputValue => ({
  type: REQUEST_USERS,
  payload: inputValue,
});

export const receiveUsers = payload => ({
  type: RECEIVE_USERS,
  payload,
});

export const handleUserClickEvent = username => ({
  type: REQUEST_USER_DATA,
  payload: { username },
});

export const receiveUserData = payload => ({
  type: RECEIVE_USER_DATA,
  payload,
});

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    ...fragment.data,
  },
});

const cache = new InMemoryCache({ fragmentMatcher });
const authLink = setContext((_, { headers }) => {
  const { token } = JSON.parse(localStorage.getItem('auth'));
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});
const httpLink = createHttpLink({ uri: 'https://api.github.com/graphql' });
const client = new ApolloClient({ link: authLink.concat(httpLink), cache });

const querySearchUsers = username => ({
  query: gql`
    {
      search(type: USER, query: ${username}, first: 30) {
        userCount
        nodes {
          ... on User {
            login,
            id
          }
        }
      }
    }
  `,
});

const queryUserAndStarredRepos = ({ username }) => ({
  query: gql`
    {
      user(login: ${username}) {
        bio,
        login,
        name,
        company,
        email,
        url,
        avatarUrl,
        location,
        starredRepositories(first: 10, orderBy:{field: STARRED_AT, direction: DESC}) {
          totalCount,
          pageInfo {
            endCursor,
            hasNextPage
          },
          edges {
            node {
              id,
              description,
              nameWithOwner,
              url,
              viewerHasStarred,
              stargazers(after: null) {
                totalCount
              }
            }
          }
        }
      }
    }
  `,
});

export const fetchUsersEpic = action$ => action$.pipe(
  ofType(REQUEST_USERS),
  debounceTime(300),
  filter(action => action.payload),
  switchMap(action => from(client.query(querySearchUsers(action.payload))).pipe(
    map(response => receiveUsers(response.data.search.nodes.filter(node => node.__typename === 'User'))),
  )),
);

export const fetchUserDataEpic = action$ => action$.pipe(
  ofType(REQUEST_USER_DATA),
  switchMap(action => from(client.query(queryUserAndStarredRepos(action.payload))).pipe(
    map((response) => {
      const {
        bio,
        login,
        name,
        company,
        email,
        url,
        avatarUrl,
        location,
        starredRepositories: { edges, pageInfo },
      } = response.data.user;
      return receiveUserData({
        profile: {
          bio,
          login,
          name,
          company,
          email,
          url,
          avatarUrl,
          location,
        },
        repoList: {
          pageInfo,
          repos: edges,
        },
      });
    }),
  )),
);
