const initialState = [];

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUEST_USERS':
      return action.payload ? state : [];
    case 'RECEIVE_USERS':
      return [...action.payload];
    case 'REQUEST_USER_DATA':
      return [];
    default:
      return state;
  }
};

export default searchReducer;
