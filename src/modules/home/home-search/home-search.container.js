import { connect } from 'react-redux';
import { handleSearchEvent, handleUserClickEvent } from './home-search.action';
import HomeSearch from './home-search.component';

const mapStateToProps = state => ({
  users: state.home.users,
  profile: state.home.profile,
  myUser: state.auth.myUser,
});

const actions = { handleSearchEvent, handleUserClickEvent };

export default connect(mapStateToProps, actions)(HomeSearch);
