import React, { Component, Fragment } from 'react';
import AuthButton from '../../shared/auth-button/auth-button.component';
import './home-search.style.scss';

export default class HomeSearch extends Component {
  constructor(props) {
    super(props);
    this.state = { show: false, hover: false };
  }

  handleBlur() {
    this.setState({ show: false });
  }

  handleFocus() {
    this.setState({ show: true });
  }

  handleHover(userId) {
    this.setState({ hover: userId });
  }

  handleMouseDown(username) {
    const { handleUserClickEvent } = this.props;
    handleUserClickEvent(username);
    this.inputEl.value = username;
  }

  hasProfile() {
    const { profile } = this.props;
    return Object.keys(profile).length > 0;
  }

  render() {
    const { handleSearchEvent, users, myUser } = this.props;
    const classBox = !this.hasProfile() ? 'd-flex align-items-center flex-column height-full-80' : 'media mt-4';
    const classLogo = !this.hasProfile() ? 'mt-auto mb-4' : 'align-self-center mr-5';
    const classSearch = !this.hasProfile() ? 'w-50 mb-auto position-relative' : 'media-body align-self-center position-relative';
    const { show, hover } = this.state;
    return (
      <Fragment>
        <div className={classBox}>
          <div className={classLogo}>
            <h1 className="h2 font-weight-light" styleName="logo">
              Github
              <span styleName="logo-stars">
                Stars
              </span>
            </h1>
          </div>
          <div className={classSearch} styleName={!this.hasProfile() ? 'box-search' : ''}>
            <div className="input-group">
              <input
                disabled={Object.keys(myUser).length === 0}
                placeholder="Digite um nome de usuário"
                autoComplete="off"
                className="form-control"
                onBlur={() => this.handleBlur()}
                onFocus={() => this.handleFocus()}
                onChange={e => handleSearchEvent(e.target.value)}
                ref={(input) => { this.inputEl = input; }}
              />
            </div>
            { show && (
              <ul className="list-group" styleName="list-group">
                { users.map(user => (
                  <li
                    className={`c-pointer list-group-item p-0${hover === user.id ? ' active' : ''}`}
                    key={user.id}
                    onMouseEnter={() => this.handleHover(user.id)}
                    onMouseLeave={() => this.handleHover(false)}
                  >
                    <div onMouseDown={() => this.handleMouseDown(user.login)} role="button" tabIndex="0" className="px-3 py-3">
                      {user.login}
                    </div>
                  </li>))
                }
              </ul>)
            }
          </div>
          { (this.hasProfile() && myUser.login) && (
            <div className="align-self-center ml-3">
              <img src={myUser.avatar_url} width="50" alt="avatar" className="rounded-circle" />
            </div>
          )}
        </div>
        <AuthButton />
      </Fragment>
    );
  }
}
