import { combineEpics } from 'redux-observable';
import { fetchUserDataEpic, fetchUsersEpic } from './home-search/home-search.action';
import {
  fetchUpdateStarredRepoEpic,
  fetchMoreUserDataEpic,
} from './home-list/home-list.action';

export default combineEpics(
  fetchUsersEpic,
  fetchUserDataEpic,
  fetchUpdateStarredRepoEpic,
  fetchMoreUserDataEpic,
);
