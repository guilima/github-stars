import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NotFound from '../not-found.component';

import Home from './home.component';

const HomeModule = ({ match }) => (
  <Switch>
    <Route path={match.url} exact component={Home} />
    <Route component={NotFound} />
  </Switch>
);

export default HomeModule;
