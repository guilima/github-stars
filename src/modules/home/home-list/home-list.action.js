import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';
import { ofType } from 'redux-observable';
import { of, from } from 'rxjs';
import {
  switchMap,
  map,
  filter,
  catchError,
} from 'rxjs/operators';

export const REQUEST_MORE_USER_DATA = 'REQUEST_MORE_USER_DATA';
export const RECEIVE_MORE_USER_DATA = 'RECEIVE_MORE_USER_DATA';
export const UPDATE_STARRED_REPO = 'UPDATE_STARRED_REPO';
export const SUCCESS_UPDATE_STARRED_REPO = 'SUCCESS_UPDATE_STARRED_REPO';
export const FAIL_UPDATE_STARRED_REPO = 'FAIL_UPDATE_STARRED_REPO';
export const CLEAR_UPDATE_STARRED_REPO_ERROR = 'CLEAR_UPDATE_STARRED_REPO_ERROR';

export const requestMoreUserData = (username, offset) => ({
  type: REQUEST_MORE_USER_DATA,
  payload: { username, offset },
});

export const receiveMoreUserData = payload => ({
  type: RECEIVE_MORE_USER_DATA,
  payload,
});

export const updateStarredRepo = (viewerHasStarred, repoId) => ({
  type: UPDATE_STARRED_REPO,
  payload: { viewerHasStarred, repoId },
});

export const successUpdateStarredRepo = payload => ({
  type: SUCCESS_UPDATE_STARRED_REPO,
  payload,
});

export const failUpdateStarredRepo = payload => ({
  type: FAIL_UPDATE_STARRED_REPO,
  payload,
});

export const clearUpdateStarredRepoError = () => ({
  type: CLEAR_UPDATE_STARRED_REPO_ERROR,
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const { token } = JSON.parse(localStorage.getItem('auth'));

  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});
const httpLink = createHttpLink({ uri: 'https://api.github.com/graphql' });
const client = new ApolloClient({ link: authLink.concat(httpLink), cache: new InMemoryCache() });

const mutationRemoveStar = repoId => ({
  variables: { input: { starrableId: repoId } },
  mutation: gql`
    mutation RemoveStar($input: RemoveStarInput!) {
      removeStar(input: $input) {
        starrable {
          id,
          viewerHasStarred,
          stargazers(after: null) {
            totalCount
          }
        }
      }
    }
  `,
});

const mutationAddStar = repoId => ({
  variables: { input: { starrableId: repoId } },
  mutation: gql`
    mutation AddStar($input: AddStarInput!) {
      addStar(input: $input) {
        starrable {
          id,
          viewerHasStarred,
          stargazers(after: null) {
            totalCount
          }
        }
      }
    }
  `,
});

const queryMoreStarredRepos = ({ username, offset = null }) => ({
  query: gql`
    {
      user(login: "${username}") {
        starredRepositories(first: 10, after: "${offset}", orderBy:{field: STARRED_AT, direction: DESC}) {
          totalCount,
          pageInfo {
            endCursor,
            hasNextPage
          },
          edges {
            cursor,
            node {
              id,
              description,
              nameWithOwner,
              url,
              viewerHasStarred,
              stargazers(after: null) {
                totalCount
              }
            }
          }
        }
      }
    }
  `,
});

export const fetchUpdateStarredRepoEpic = action$ => action$.pipe(
  ofType(UPDATE_STARRED_REPO),
  filter(action => action.payload.repoId),
  switchMap(action => from(client.mutate(
    action.payload.viewerHasStarred
      ? mutationRemoveStar(action.payload.repoId)
      : mutationAddStar(action.payload.repoId),
  )).pipe(
    map((response) => {
      const { starrable } = response.data.addStar || response.data.removeStar;
      return successUpdateStarredRepo(starrable);
    }),
    catchError(error => of(failUpdateStarredRepo(error.graphQLErrors))),
  )),
);

export const fetchMoreUserDataEpic = action$ => action$.pipe(
  ofType(REQUEST_MORE_USER_DATA),
  switchMap(action => from(client.query(queryMoreStarredRepos(action.payload))).pipe(
    map((response) => {
      const { starredRepositories: { edges, pageInfo } } = response.data.user;
      return receiveMoreUserData({
        repoList: {
          pageInfo,
          repos: edges,
        },
      });
    }),
  )),
);
