import React, { Fragment } from 'react';
import './home-list.style.scss';
import Octicon from '../../octicon.component';
import Alert from '../../shared/alert/alert';

const HomeList = ({
  repoList,
  profile,
  updateStarredRepo,
  requestMoreUserData,
  clearUpdateStarredRepoError,
}) => {
  const { repos, pageInfo, error } = repoList;
  if (repos.length <= 0) {
    return <Fragment />;
  }
  return (
    <section>
      {error && (
        <Alert message={error.map(item => item.message)} handle={clearUpdateStarredRepoError} />
      )}
      {repos.map((repo, index, arr) => (
        <div key={repo.node.id} className={`w-100 d-inline-block card mb-2 mt-2${index === 0 ? ' mt-3' : ''}${arr.length - 1 === index ? ' mb-3' : ''}`}>
          <div className="card-body">
            <div className="media">
              <div className="media-body align-self-center position-relative">
                <h2 className="card-title h5 font-weight-normal">
                  {repo.node.nameWithOwner}
                </h2>
                <p className="card-text font-weight-light">
                  {repo.node.description}
                </p>
                <Octicon name="star" size={1.1} />
                <span className="ml-1 font-weight-light">
                  {repo.node.stargazers.totalCount}
                </span>
              </div>
              <div className="align-self-center ml-4">
                <button
                  className={`btn btn-primary ${!repo.node.viewerHasStarred ? 'btn-outline-primary' : ''}`}
                  type="button"
                  onClick={() => updateStarredRepo(repo.node.viewerHasStarred, repo.node.id)}
                >
                  { !repo.node.viewerHasStarred ? 'star' : 'unstar' }
                </button>
              </div>
            </div>
          </div>
        </div>))
      }
      { pageInfo.hasNextPage && (
        <button type="button" className="align-items-center btn btn-primary d-flex m-auto" onClick={() => requestMoreUserData(profile.login, pageInfo.endCursor)}>
          Carregar mais
          { pageInfo.loading && (
            <span className="d-flex ml-2 spin">
              <Octicon name="sync" className="spin" />
            </span>)
          }
        </button>)
      }
    </section>
  );
};

export default HomeList;
