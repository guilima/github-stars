const initialState = {
  pageInfo: {
    endCursor: null,
    hasNextPage: false,
    loading: false,
  },
  repos: [],
  error: false,
};

const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RECEIVE_USER_DATA':
      return {
        ...state,
        pageInfo: { ...action.payload.repoList.pageInfo, loading: false },
        repos: [...action.payload.repoList.repos],
      };
    case 'REQUEST_MORE_USER_DATA':
      return { ...state, pageInfo: { ...state.pageInfo, loading: true } };
    case 'RECEIVE_MORE_USER_DATA':
      return {
        ...state,
        pageInfo: { ...action.payload.repoList.pageInfo, loading: false },
        repos: [...state.repos, ...action.payload.repoList.repos],
      };
    case 'SUCCESS_UPDATE_STARRED_REPO':
      return {
        ...state,
        repos: [
          ...state.repos.map(repo => (repo.node.id !== action.payload.id
            ? repo
            : {
              ...repo,
              node: {
                ...repo.node,
                viewerHasStarred: action.payload.viewerHasStarred,
                stargazers: action.payload.stargazers,
              },
            })),
        ],
        error: false,
      };
    case 'FAIL_UPDATE_STARRED_REPO':
      return { ...state, error: action.payload };
    case 'CLEAR_UPDATE_STARRED_REPO_ERROR':
      return { ...state, error: false };
    default:
      return state;
  }
};

export default listReducer;
