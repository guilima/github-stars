import { connect } from 'react-redux';
import { updateStarredRepo, requestMoreUserData, clearUpdateStarredRepoError } from './home-list.action';
import HomeList from './home-list.component';

const mapStateToProps = state => ({
  profile: state.home.profile,
  repoList: state.home.repoList,
  auth: state.auth,
});

const actions = { updateStarredRepo, requestMoreUserData, clearUpdateStarredRepoError };

export default connect(mapStateToProps, actions)(HomeList);
