import React from 'react';
import './home.style.scss';
import HomeSearch from './home-search/home-search.container';
import HomeSidebar from './home-sidebar/home-sidebar.container';
import HomeList from './home-list/home-list.container';

const Home = () => (
  <section className="container">
    <div className="row">
      <div className="col-md-12">
        <HomeSearch />
      </div>
    </div>
    <div className="media mt-5">
      <div className="align-self-top mt-4 shadow bg-white rounded" styleName="sidebar">
        <HomeSidebar />
      </div>
      <div className="media-body px-4 py-2 shadow-lg bg-white rounded mb-5" styleName="list">
        <HomeList />
      </div>
    </div>
  </section>
);

export default Home;
