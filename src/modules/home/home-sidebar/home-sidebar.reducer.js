const initialState = {};

const sidebarReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RECEIVE_USER_DATA':
      return { ...action.payload.profile };
    default:
      return state;
  }
};

export default sidebarReducer;
