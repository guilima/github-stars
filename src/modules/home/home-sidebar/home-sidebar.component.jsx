import React, { Fragment } from 'react';
import './home-sidebar.style.scss';
import Octicon from '../../octicon.component';

const HomeSidebar = ({ profile }) => {
  if (!profile.login) {
    return <Fragment />;
  }
  return (
    <div>
      <figure className="figure text-center w-100 px-3 py-4 mb-0" styleName="sidebar-avatar">
        <img src={profile.avatarUrl} className="figure-img rounded-circle" styleName="profile-image" alt="User profile" />
        <figcaption>
          {profile.name}
          <br />
          <small>
            {profile.login}
          </small>
        </figcaption>
      </figure>
      <div className="px-3 py-4" styleName="sidebar-bio">
        <p>
          {profile.bio}
        </p>
        <ul className="list-unstyled">
          { profile.company && (
            <li className="text-truncate">
              <i className="mr-1" styleName="icon">
                <Octicon name="organization" />
              </i>
              {profile.company}
            </li>)
          }
          { profile.location && (
            <li className="text-truncate">
              <i className="mr-1" styleName="icon">
                <Octicon name="location" />
              </i>
              {profile.location}
            </li>)
          }
          { profile.email && (
            <li className="text-truncate">
              <i className="mr-1" styleName="icon">
                <Octicon name="mail" />
              </i>
              {profile.email}
            </li>)
          }
          { profile.html_url && (
            <li className="text-truncate">
              <i className="mr-1" styleName="icon">
                <Octicon name="globe" />
              </i>
              <a href={profile.url} styleName="link">
                {profile.url}
              </a>
            </li>)
          }
        </ul>
      </div>
    </div>
  );
};

export default HomeSidebar;
