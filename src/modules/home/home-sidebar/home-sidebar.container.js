import { connect } from 'react-redux';
import HomeSidebar from './home-sidebar.component';

const mapStateToProps = state => ({ profile: state.home.profile });

export default connect(mapStateToProps)(HomeSidebar);
