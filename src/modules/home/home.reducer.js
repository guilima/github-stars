import { combineReducers } from 'redux';
import listReducer from './home-list/home-list.reducer';
import searchReducer from './home-search/home-search.reducer';
import sidebarReducer from './home-sidebar/home-sidebar.reducer';

export default combineReducers({
  users: searchReducer,
  repoList: listReducer,
  profile: sidebarReducer,
});
