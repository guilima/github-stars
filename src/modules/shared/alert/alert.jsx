import React from 'react';
import './alert.style.scss';

const Alert = ({ message, handle }) => (
  <div styleName="alert">
    <span
      styleName="closebtn"
      onClick={() => handle()}
      onKeyPress={e => e.key === 'Enter' && handle()}
      role="button"
      tabIndex="0"
    >
      &times;
    </span>
    {message}
  </div>
);

export default Alert;
