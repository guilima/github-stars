import { connect } from 'react-redux';
import React, { Component } from 'react';
import './auth-button.style.scss';

class AuthButton extends Component {
  constructor(props) {
    super(props);
    this.authGithubLoginUrl = 'https://github.com/login/oauth/authorize?client_id=4c261b52f45e86c7e4df&scope=read:user,public_repo';
  }

  handleClick() {
    window.open(this.authGithubLoginUrl, '_self');
  }

  render() {
    const { token } = this.props;
    return (
      !token && (
        <div className="text-center mt-4">
          <button type="button" className="button button-primary" onClick={() => this.handleClick()}>
            Github Login
          </button>
          <p className="mb-0">
            <small>
              Necessário para interação
            </small>
          </p>
        </div>)
    );
  }
}

const mapStateToProps = state => ({ ...state.auth });
export default connect(mapStateToProps)(AuthButton);
