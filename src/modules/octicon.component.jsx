import React, { Fragment } from 'react';
import * as octicons from 'octicons';

const Octicon = ({ name, size = 1 }) => {
  const {
    width,
    height,
    version,
    path,
    options: {
      class: iconClass,
      viewBox,
    },
  } = octicons[name];
  const attrValue = /fill-rule="(.*?)"\sd="(.*?)"/gm.exec(path);
  return (
    <Fragment>
      <svg version={version} width={width * size} height={height * size} viewBox={viewBox} className={iconClass} aria-hidden="true">
        <path fillRule={attrValue[1]} d={attrValue[2]} />
      </svg>
    </Fragment>
  );
};

export default Octicon;
