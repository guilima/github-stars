import React from 'react';
import { Link } from 'react-router-dom';
import Octicon from './octicon.component';

const NotFound = () => (
  <section className="container d-flex align-items-center flex-column text-center height-full-80">
    <h1 className="mt-auto mb-4">
      404
      <br />
      <span>
        Página não encontrada
      </span>
    </h1>
    <Link to="/" className="btn btn-primary mb-auto">
      <span className="d-flex align-items-center">
        <Octicon name="chevron-left" size={1.5} />
        Retorne para a página inicial
      </span>
    </Link>
  </section>
);

export default NotFound;
